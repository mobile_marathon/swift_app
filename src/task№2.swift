let daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

let monthNames = ["Январь", "Февраль", "Март", "Апрель",
                  "Май", "Июнь", "Июль", "Август",
                  "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]

// Выводим количество дней в каждом месяце
for days in daysInMonths {
    print("\(days) ", terminator: "")
}
print()
// Выводим название месяца и количество дней в нем
for i in 0..<daysInMonths.count {
    let month = monthNames[i]
    let days = daysInMonths[i]
    print("\(month): \(days)  ", terminator: "")
}
print()
// Используем массив кортежей
let monthsAndDays = [("Январь", 31), ("Февраль", 28), ("Март", 31), ("Апрель", 30),
                     ("Май", 31), ("Июнь", 30), ("Июль", 31), ("Август", 31),
                     ("Сентябрь", 30), ("Октябрь", 31), ("Ноябрь", 30), ("Декабрь", 31)]

// Выводим из массива кортежей
for monthAndDays in monthsAndDays {
    let month = monthAndDays.0
    let days = monthAndDays.1
    print("\(month): \(days)  ", terminator: "")
}
print()
// Выводим количество дней в каждом месяце в обратном порядке
for i in (0..<daysInMonths.count).reversed() {
    let month = monthNames[i]
    let days = daysInMonths[i]
   print("\(month): \(days)  ", terminator: "")
}
print()
// Вычисляем количество дней до произвольной даты (9 июля)
let targetMonth = 7
let targetDay = 9
var daysCount = 0

for i in 0..<targetMonth {
    daysCount += daysInMonths[i]
}

daysCount += targetDay - 1

print("Количество дней до \(targetDay) \(monthNames[targetMonth-1]): \(daysCount)")