// Массив для сортировки
var numbers = [2, 6, 1, 9, 3, 8, 5, 7, 4]

// Сортировка в порядке возрастания
numbers.sort(by: { $0 < $1 })
print(numbers)

// Сортировка в порядке убывания
numbers.sort(by: { $0 > $1 })
print(numbers)

// Массив имен друзей
var friendNames = ["Tom", "Alice", "Bob", "Cindy", "David"]

// Сортировка по количеству букв в имени
friendNames.sort(by: { $0.count < $1.count })

// Создание словаря с ключом - количеством букв в имени и значением - именем друга
var friendDict = [Int: String]()
for name in friendNames {
    friendDict[name.count] = name
}

// Функция для вывода ключа и значения
func printKeyValue(key: Int, value: String) {
    print("Ключ: \(key), Значение: \(value)")
}

// Вызов функции для вывода ключа и значения из словаря
if let friend = friendDict[3] {
    printKeyValue(key: 3, value: friend)
}

var strArr = ["Hello", "World"]
var numArr = [1,3,4]

if strArr.isEmpty {
    strArr.append("Default String")
}

if numArr.isEmpty {
    numArr.append(0)
}

print(strArr)
print(numArr)
print(friendNames)
