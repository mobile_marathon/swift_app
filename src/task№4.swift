//enum с типом int
enum Number: Int {
    case one = 1
    case two
}
//enum с типом string
enum Letter: String {
    case a = "A"
    case b = "B"
}
//enum пол
enum sex {
    case male
    case female
}
//enum категория возраста
enum AgeCategory {
    case young
    case middleAged
    case elderly
}
//enum стаж
enum Experience {
    case none
    case beginner
    case intermediate
    case advanced
}
//enum цвета радуги
enum RainbowColor {
    case red
    case orange
    case yellow
    case green
    case blue
    case indigo
    case violet
}

func printEnumCases() {
    enum Direction {
        case green
        case yellow
        case red
        case blue
    }
    
    enum Fruit {
        case apple
        case banana
        case cherry
        case ice
    }
    
    let direction = Direction.red
    let fruit = Fruit.cherry
    
    switch (direction, fruit) {
    case (.green, .apple):
        print("apple green")
    case (.yellow, .banana):
        print("banana yellow")
    case (.red, .cherry):
        print("cherry red")
    case (.blue, .ice):
        print("ice blue")
    default:
        print("unknown case")
    }
}

// вызов функции, которая выставляет оценки ученикам в школе
printEnumCases()

enum Score: String {
    case A = "Отлично"
    case B = "Хорошо"
    case C = "Удовлетворительно"
    case D = "Неудовлетворительно"
}

func getScoreValue(score: Score) -> Int {
    switch score {
    case .A:
        return 5
    case .B:
        return 4
    case .C:
        return 3
    case .D:
        return 2
    }
}

// вызов функции, которая выводит в консоль какие автомобили стоят в гараже
let myScore = Score.A
let myScoreValue = getScoreValue(score: myScore)
print("Оценка \(myScore.rawValue) соответствует значению \(myScoreValue)")

// Enum с перечислением доступных автомобилей
enum Car {
    case sedan
    case lada
    case truck
    case sports
}

// Метод, выводящий все доступные автомобили в консоль
func printGarage() {
    let garage: [Car] = [.sedan, .lada, .truck, .sports]
    
    for car in garage {
        switch car {
        case .sedan:
            print("sedan in the garage.")
        case .lada:
            print("lada in the garage.")
        case .truck:
            print("truck in the garage.")
        case .sports:
            print("sports car in the garage.")
        }
    }
}

// Вызов метода
printGarage()