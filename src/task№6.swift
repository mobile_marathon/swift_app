final class IOSCollection<Element> {
    private var elements: [Element]

    init(elements: [Element]) {
        self.elements = elements
    }

    func append(_ element: Element) {
        copyIfNeeded()
        elements.append(element)
    }


    subscript(index: Int) -> Element {
        get {
            return elements[index]
        }
        set {
            copyIfNeeded()
            elements[index] = newValue
        }
    }

    private func copyIfNeeded() {
        if !isKnownUniquelyReferenced(&elements) {
            elements = Array(elements)
        }
    }
}
protocol Hotel {
    var roomCount: Int { get set }
    
    init(roomCount: Int)
}

class HotelAlfa: Hotel {
    var roomCount: Int
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}
//___________________________________

protocol GameDice {
    var numberDice: Int { get }
}

extension Int: GameDice {
    var numberDice: Int {
        return self
    }
}

let diceCoub = 4
print("Выпало \(diceCoub.numberDice) на кубике")
//___________________________________

protocol Protocol {
    var Option1: String { get }
    var Option2: Int? { get set }
    func method()
}

class Class: Protocol {
    var Option1: String { return "Hi" }
    var Option2: Int?

    func method() {
        print("it`s method called.")
    }
}

let test = Class()
print(test.Option1) 
test.method() 
//___________________________
protocol Coding {
    var time: Int { get set }
    var linesOfCode: Int { get set }
    func writeCode(platform: Platform, numberOfSpecialist: Int);
    func stopCoding()
}

protocol Testing {
    var numberOfTesters: Int { get set }
    func testCode(platform: Platform)
}

enum Platform {
    case ios
    case android
    case web
}

class Company: Coding, Testing {
    var numberOfProgrammers: Int
    var iosSpecialists: Int
    var androidSpecialists: Int
    var webSpecialists: Int
    var time: Int = 0
    var linesOfCode: Int = 0
    var numberOfTesters: Int = 0
    
    init(numberOfProgrammers: Int, iosSpecialists: Int, androidSpecialists: Int, webSpecialists: Int, numberOfTesters: Int) {
        self.numberOfProgrammers = numberOfProgrammers
        self.iosSpecialists = iosSpecialists
        self.androidSpecialists = androidSpecialists
        self.webSpecialists = webSpecialists
        self.numberOfTesters = numberOfTesters
    }
    
    func writeCode(platform: Platform, numberOfSpecialist: Int) {
        switch platform {
        case .ios:
            if numberOfSpecialist <= iosSpecialists {
                print("Разработка началась. Пишем код для iOS")
            } else {
                print("Недостаточно iOS разработчиков")
            }
        case .android:
            if numberOfSpecialist <= androidSpecialists {
                print("Разработка началась. Пишем код для Android")
            } else {
                print("Недостаточно Android разработчиков")
            }
        case .web:
            if numberOfSpecialist <= webSpecialists {
                print("Разработка началась. Пишем код для веб-приложения")
            } else {
                print("Недостаточно веб-разработчиков")
            }
        }
    }
    
    func stopCoding() {
        print("Работа закончена. Сдаю в тестирование")
    }
    
    func testCode(platform: Platform) {
        print("Тестируем \(platform) приложение")
    }
}

let company = Company(numberOfProgrammers: 10, iosSpecialists: 3, androidSpecialists: 2, webSpecialists: 5, numberOfTesters: 2)

// company.writeCode(.ios, numberOfSpecialist: 1) // Output: Недостаточно iOS разработчиков
// company.writeCode(.android, numberOfSpecialist: 1) // Output: Разработка началась. Пишем код для Android
company.stopCoding() // Output: Работа закончена. Сдаю в тестирование
// company.testCode(.web) // Output: Тестируем web приложение
