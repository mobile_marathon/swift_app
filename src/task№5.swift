class Vehicle {
    var name: String
    var color: String
    
    init(name: String, color: String) {
        self.name = name
        self.color = color
    }
}

class Car: Vehicle {
    var numWheels: Int
    
    init(name: String, color: String, numWheels: Int) {
        self.numWheels = numWheels
        super.init(name: name, color: color)
    }
}

class Motorcycle: Vehicle {
    var hasSidecar: Bool
    
    init(name: String, color: String, hasSidecar: Bool) {
        self.hasSidecar = hasSidecar
        super.init(name: name, color: color)
    }
}

class House {
    var width: Double
    var height: Double
    var isDestroyed = false
    
    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }
    
    func create() {
        let area = width * height
        print("Площадь дома \(area) квадратных метров.")
    }
    
    func destroy() {
        isDestroyed = true
        print("Дом уничтожен.")
    }
}

// Создаем объекты класса House
let house1 = House(width: 10, height: 20)
let house2 = House(width: 15, height: 30)

// Вызываем методы объектов класса House
house1.create()
house2.destroy()

class Student {
    var name: String
    var age: Int
    var grade: Int
    
    init(name: String, age: Int, grade: Int) {
        self.name = name
        self.age = age
        self.grade = grade
    }
}

class StudentSorter {
    static func sortByName(students: [Student]) -> [Student] {
        return students.sorted { $0.name < $1.name }
    }
    
    static func sortByAge(students: [Student]) -> [Student] {
        return students.sorted { $0.age < $1.age }
    }
    
    static func sortByGrade(students: [Student]) -> [Student] {
        return students.sorted { $0.grade > $1.grade }
    }
}

// Структура для представления точки на плоскости
struct Point {
    var x: Int
    var y: Int
}

// Класс для представления прямоугольника на плоскости
class Rectangle {
    var origin: Point
    var width: Int
    var height: Int
    
    init(origin: Point, width: Int, height: Int) {
        self.origin = origin
        self.width = width
        self.height = height
    }
    
    func area() -> Int {
        return width * height
    }
}

// Создание экземпляров структуры и класса
let point = Point(x: 10, y: 20)
let rectangle = Rectangle(origin: point, width: 100, height: 200)

// Вывод площади прямоугольника
print(rectangle.area()) // 20000

// Структуры:
// структуры являются значимыми типами, они копируются при присваивании или передаче в качестве аргумента функции;
// структуры не могут наследовать свойства или методы других типов;
// структуры могут иметь инициализаторы, но не могут иметь деструкторов.

// Классы:
// классы являются ссылочными типами, они передаются по ссылке;
// классы могут наследовать свойства и методы других классов;
// классы могут иметь инициализаторы и деструкторы.


// массив со всеми возможными мастями
let suits = ["черви", "бубны", "пики", "трефы"]
// массив со всеми возможными значениями карт
let values = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "валет", "дама", "король", "туз"]

// функция для выбора случайной карты
func randomCard() -> (suit: String, value: String) {
    let randomSuit = suits.randomElement()!
    let randomValue = values.randomElement()!
    return (suit: randomSuit, value: randomValue)
}

// массив из 5 случайных карт
var cards = [randomCard(), randomCard(), randomCard(), randomCard(), randomCard()]

// сортируем карты по значению
cards.sort { values.firstIndex(of: $0.value)! < values.firstIndex(of: $1.value)! }

// проверяем наличие комбинаций
let isFlush = cards.allSatisfy { $0.suit == cards[0].suit } // флеш
let isStraight = (0..<4).allSatisfy { values.firstIndex(of: cards[$0].value)! + 1 == values.firstIndex(of: cards[$0+1].value)! } // стрит
let isStraightFlush = isFlush && isStraight // стрит флеш

// выводим результат в консоль
if isStraightFlush {
    print("вас стрит флеш")
} else if isFlush {
    print("вас флеш")
} else if isStraight {
    print("вас стрит")
} else {
    print("вас \(cards[4].value) \(cards[4].suit)")
}